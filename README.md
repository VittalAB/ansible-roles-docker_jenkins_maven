# Ansible Roles Docker_Jenkins_Maven

This repo contains ansible roles to install jenkins, maven, docker on remote ubuntu machine.

## Ansible role for installaing docker 

---
- name : install docker
  apt : 
    name : docker.io
    state : present

- name : start docker
  service : 
    name : docker
    enabled: true
    state : started


## Ansible role for installing maven and java

---

- name : install java
  apt : 
    name : openjdk-11-jdk
    state : present
    
- name : install maven 
  apt : 
    name : maven
    state : present


## Ansible  role for installing jenkins 

### To be done ......